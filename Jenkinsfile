node {
  stage('SCM') {
    checkout scm
  }

  stage('SonarQube Analysis') {
    def scannerHome = tool 'SonarScanner';
    withSonarQubeEnv() {
      sh "${scannerHome}/bin/sonar-scanner"
    }
  }

  stage('Build application image') {
    sh "cd application; docker compose build;"
  }

  stage('Test applications') {
    sh "docker container run --rm application-app go test -v"
  }

  stage('Coverage report') {
    sh 'docker container rm --force cover || true' 
    sh 'docker container run --name cover application-app bash -c "go test -coverprofile=coverage.out ./...; go tool cover -func=coverage.out; go tool cover -html=coverage.out -o /tmp/coverage.html; ls -lash /tmp/";'
    sh 'docker cp cover:/tmp/coverage.html application/'
    sh 'ls -lash application;'
    sh 'docker container rm --force cover || true' 

    publishHTML(target: [
      allowMissing: false,
      alwaysLinkToLastBuild: true,
      keepAll: true,
      reportDir: 'application',
      reportFiles: 'coverage.html',
      reportName: 'Code Coverage Report'
    ])
  }

  stage('Run integration tests') {
    echo "TODO"
  }

  stage('Deploy application') {
    sh "cd application; docker compose -f compose.yaml up -d"
  }
}