# Management Qualite

Dans ce projet nous allons utiliser d'un outil de vérification du code à des fins de fiabilisation/sécurisation de celui-ci dans
une logique d'intégration continue.  
Pour ce faire on va mettre en place une solution SonarQube qui vérifie la qualité du code, le tout automatisé avec Jenkins.  

## Infrastructure

Nous avons mis en place une machine virtuelle dans un environnement Azure.  
Cette VM à une taille de 4vCPU et 8Go de RAM.  

### Outils utilisés

| Tools   |      Version   |
|----------|:-------------:|
| Docker engine |  24.0.2 |
| Docker compose |    2.18.1   |
| Jenkins| 2.401.1-lts |
|Sonarqube| latest |
|Postgres|13|

### Déploiement des outils

Tous nos outils sont conteneurisés avec Docker. 
Un docker-compose est disponible dans le répertoire [infra](infra). Il contient 3 services, Jenkins, Sonarqube et une base de données.  

Déployer l'infrastructure:  
```bash
docker compose up -d
```
Les services seront accessible après quelques minutes:
-  Jenkins:   http://98.66.138.163:8080/
- SonarQube : http://98.66.138.163:9000/


### Déploiement de l'application

L'application est buildée automatiquement via Jenkins et déployée sur le port http du serveur.
Après déploiement l'application est accessible sur le lien: http://98.66.138.163.

Pour un déloiement manuel l'on peut suivre la procédure suivante 
```bash
git clone git@gitlab.com:FlorianD78340/management-qualite.git
cd management-qualite/application/
docker compose build
docker compose up -d
```

### Schéma de l'infrastructure déployée

![schema-infra](docs/images/test.drawio.png)

## Mise en place de l'intégration continue

Avec un [Jenkinsfile](Jenkinsfile), nous avons mis en place une pipeline d'intégration continue afin de tester le code de l'application.  
Enfin nous avons paramétré Jenkins afin qu'il puisse utiliser Sonarqube et effectuer des tests OWASP sur l'application.  

Nous avons donc obtenu le flux de travail suivant:  

```mermaid
flowchart TD
  A[Git] -->|Commit| B(Webhook);
  B --> C{Jenkins};
  C -->|Trigger| D[Pipeline];
  D -->|Trigger after pipeline| E[OWASP dependency check];

```


Ci-dessous le contenu de la pipeline de Jenkins :

```mermaid
graph TD
  A[SCM] --> B[SonarQube Analysis]
  B --> C[Build application image]
  C --> D[Test applications]
  D --> E[Coverage report]
  E --> F[Run integration tests]
  F --> G[Deploy application]
```
## TODO

Voir issues avec le label 'ToDo'.
