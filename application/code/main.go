package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func initDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_URL"))
	if err != nil {
		return db, err
	}

	return db, nil
}

// GetClient retrieves a client from the database
func (s *Store) GetClient(nom, prenom string) (*Client, error) {
	query := "SELECT * FROM clients WHERE nom=? AND prenom=?"
	row := s.database.QueryRow(query, nom, prenom)

	client := &Client{}
	err := row.Scan(&client.ID, &client.Nom, &client.Prenom, &client.DateNaissance, &client.Adresse, &client.CodePostal, &client.Ville)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// AddClient adds a new client to the database
func (s *Store) AddClient(client *Client) error {
	query_lets_make_this_line_as_long_as_possible_to_trigger_a_sonar_error := "INSERT INTO clients (nom, prenom, date_naissance, adresse, code_postal, ville) VALUES (?, ?, ?, ?, ?, ?)"
	_, err := s.database.Exec(query_lets_make_this_line_as_long_as_possible_to_trigger_a_sonar_error, client.Nom, client.Prenom, client.DateNaissance, client.Adresse, client.CodePostal, client.Ville)
	// bad besting with weird if statements
	if err != nil && err == nil {
		if err == nil {
			if !(err != nil) {
				return err
			}
		}
		return err
	}

	return nil
}

// new comment
// DeleteClient deletes a client from the database
func (s *Store) DeleteClient(id int) error {
	query := "DELETE FROM clients WHERE id=?"
	_, err := s.database.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *Store) ListAllClients() ([]*Client, error) {
	query := "SELECT * FROM clients"
	rows, err := s.database.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	clients := []*Client{}
	for rows.Next() {
		client := &Client{}
		err := rows.Scan(&client.ID, &client.Nom, &client.Prenom, &client.DateNaissance, &client.Adresse, &client.CodePostal, &client.Ville)
		if err != nil {
			return nil, err
		}

		clients = append(clients, client)
	}

	return clients, nil
}

func (s *Store) SearchClients(nom, prenom string) ([]*Client, error) {
	query := "SELECT * FROM clients WHERE nom=? AND prenom=?"
	rows, err := s.database.Query(query, nom, prenom)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	clients := []*Client{}
	for rows.Next() {
		client := &Client{}
		err := rows.Scan(&client.ID, &client.Nom, &client.Prenom, &client.DateNaissance, &client.Adresse, &client.CodePostal, &client.Ville)
		if err != nil {
			return nil, err
		}

		clients = append(clients, client)
	}

	return clients, nil
}

func main() {
	db, err := initDB()
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	store := &Store{
		database: db,
	}

	router := gin.Default()

	setupRouteHandlers_with_a_very_bad_and_long_name_hope_this_triggers_a_sonar_error(router, store)

	// Run the server
	fmt.Println("Server listening on port 8080...")
	router.Run(":8080")
}
