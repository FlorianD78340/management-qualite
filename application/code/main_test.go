package main

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/suite"
)

type StoreSuite struct {
	suite.Suite
	db     *sql.DB
	mock   sqlmock.Sqlmock
	store  *Store
	router *gin.Engine
}

func (suite *StoreSuite) SetupTest() {
	db, mock, err := sqlmock.New()
	suite.Require().NoError(err)

	suite.db = db
	suite.mock = mock

	suite.store = &Store{database: db}

	// Create a test router using Gin
	router := gin.Default()

	setupRouteHandlers_with_a_very_bad_and_long_name_hope_this_triggers_a_sonar_error(router, suite.store)

	suite.router = router
}

func (suite *StoreSuite) TearDownTest() {
	suite.Require().NoError(suite.mock.ExpectationsWereMet())
	// suite.Require().NoError(suite.db.Close())
}

func (suite *StoreSuite) TestGetClientHandler() {
	// Set up the mock database response
	rows := sqlmock.NewRows([]string{"id", "nom", "prenom", "date_naissance", "adresse", "code_postal", "ville"}).
		AddRow(1, "Doe", "John", "1990-01-01", "123 Street", "12345", "City").
		AddRow(2, "Smith", "Jane", "1992-05-10", "456 Avenue", "67890", "Town")
	suite.mock.ExpectQuery("^SELECT (.+) FROM clients$").WillReturnRows(rows)

	// Create a test request to the GetClientHandler endpoint
	req, err := http.NewRequest("GET", "/", nil)
	suite.Require().NoError(err)

	rec := httptest.NewRecorder()

	suite.router.ServeHTTP(rec, req)

	// Check the response status code
	suite.Require().Equal(http.StatusOK, rec.Code)

	// Check the response body
	expectedBody := `<td>1</td><td>Doe</td><td>John</td><td>1990-01-01</td><td>123 Street</td><td>12345</td><td>City</td>`
	suite.Require().Contains(strings.TrimSpace(rec.Body.String()), expectedBody)

	expectedBody = `<td>1</td><td>Doe</td><td>John</td><td>1990-01-01</td><td>123 Street</td><td>12345</td><td>City</td>`
	suite.Require().Contains(strings.TrimSpace(rec.Body.String()), expectedBody)
}

// Add more test functions for other routes...
func (suite *StoreSuite) TestSearchClientsHandler() {
	// Set up the mock database response
	rows := sqlmock.NewRows([]string{"id", "nom", "prenom", "date_naissance", "adresse", "code_postal", "ville"}).
		AddRow(1, "Doe", "John", "1990-01-01", "123 Street", "12345", "City")
	suite.mock.ExpectQuery("^SELECT (.+) FROM clients WHERE nom=(.+) AND prenom=(.+)$").
		WithArgs("Doe", "John").
		WillReturnRows(rows)

	// Create a test request to the SearchClientsHandler endpoint with form data
	formData := url.Values{"nom": {"Doe"}, "prenom": {"John"}}
	req, err := http.NewRequest("POST", "/search", strings.NewReader(formData.Encode()))
	suite.Require().NoError(err)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	rec := httptest.NewRecorder()

	suite.router.ServeHTTP(rec, req)

	// Check the response status code
	suite.Require().Equal(http.StatusOK, rec.Code)

	// Check the response body
	expectedBody := `<td>1</td><td>Doe</td><td>John</td><td>1990-01-01</td><td>123 Street</td><td>12345</td><td>City</td>`

	// Verify that the response body contains the expected HTML
	suite.Require().Contains(rec.Body.String(), expectedBody)
}

func (suite *StoreSuite) TestAddClientHandler() {
	suite.mock.ExpectExec("INSERT INTO clients (.+)").
		WithArgs("Doe", "John", "1990-01-01", "123 Street", "12345", "City").
		WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows([]string{"id", "nom", "prenom", "date_naissance", "adresse", "code_postal", "ville"}).
		AddRow(1, "Doe", "John", "1990-01-01", "123 Street", "12345", "City")
	suite.mock.ExpectQuery("^SELECT (.+) FROM clients$").WillReturnRows(rows)

	// Create a test request to the AddClientHandler endpoint with form data
	formData := url.Values{
		"nom":            {"Doe"},
		"prenom":         {"John"},
		"date_naissance": {"1990-01-01"},
		"adresse":        {"123 Street"},
		"code_postal":    {"12345"},
		"ville":          {"City"},
	}
	req, err := http.NewRequest("POST", "/add", strings.NewReader(formData.Encode()))
	suite.Require().NoError(err)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	rec := httptest.NewRecorder()

	suite.router.ServeHTTP(rec, req)

	// Check the response status code
	suite.Require().Equal(http.StatusSeeOther, rec.Code)

	// Verify that the client was added to the store
	clients, err := suite.store.ListAllClients()
	suite.Require().NoError(err)
	suite.Require().Len(clients, 1)
	suite.Require().Equal("Doe", clients[0].Nom)
	suite.Require().Equal("John", clients[0].Prenom)
	suite.Require().Equal("1990-01-01", clients[0].DateNaissance)
	suite.Require().Equal("123 Street", clients[0].Adresse)
	suite.Require().Equal("12345", clients[0].CodePostal)
	suite.Require().Equal("City", clients[0].Ville)
}

func (suite *StoreSuite) TestDeleteClientHandler() {
	// Create a mock database response with a single row
	// rows := sqlmock.NewRows([]string{"id", "nom", "prenom", "date_naissance", "adresse", "code_postal", "ville"}).
	// 	AddRow(1, "Doe", "John", "1990-01-01", "123 Street", "12345", "City")
	// suite.mock.ExpectQuery("^SELECT (.+) FROM clients$").
	// 	WithArgs(1).
	// 	WillReturnRows(rows)

	// Expect the DELETE query to be executed
	suite.mock.ExpectExec("^DELETE FROM clients WHERE id=(.+)$").
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(0, 1)) // Return a result indicating one row affected

	// Create a test request to the DeleteClientHandler endpoint
	req, err := http.NewRequest("GET", "/delete/1", nil)
	suite.Require().NoError(err)

	rec := httptest.NewRecorder()

	suite.router.ServeHTTP(rec, req)

	// Check the response status code
	suite.Require().Equal(http.StatusSeeOther, rec.Code)

	// Verify that the DELETE query was executed
	err = suite.mock.ExpectationsWereMet()
	suite.Require().NoError(err)
}

func TestStoreSuite(t *testing.T) {
	suite.Run(t, new(StoreSuite))
}
