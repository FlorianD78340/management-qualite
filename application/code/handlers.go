package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func ListClientsHandler(c *gin.Context, s *Store) {
	clients, err := s.ListAllClients()
	if err != nil {
		log.Println(err)
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{"message": "An error occurred"})
		return
	}

	c.HTML(http.StatusOK, "index.html", gin.H{"clients": clients})
}

func SearchClientsHandler(c *gin.Context, s *Store) {
	nom := c.PostForm("nom")
	prenom := c.PostForm("prenom")

	clients, err := s.SearchClients(nom, prenom)
	if err != nil {
		log.Println(err)
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{"message": "An error occurred"})
		return
	}

	c.HTML(http.StatusOK, "index.html", gin.H{"clients": clients})
}
func AddClientHandler(c *gin.Context, s *Store) {
	nom := c.PostForm("nom")
	prenom := c.PostForm("prenom")
	dateNaissance := c.PostForm("date_naissance")
	adresse := c.PostForm("adresse")
	codePostal := c.PostForm("code_postal")
	ville := c.PostForm("ville")

	client := &Client{
		Nom:           nom,
		Prenom:        prenom,
		DateNaissance: dateNaissance,
		Adresse:       adresse,
		CodePostal:    codePostal,
		Ville:         ville,
	}

	err := s.AddClient(client)
	if err != nil {
		log.Println(err)
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{"message": "An error occurred"})
		return
	}

	c.Redirect(http.StatusSeeOther, "/")
}

func DeleteClientHandler(c *gin.Context, s *Store) {
	id := c.Param("id")

	// Convert the ID to an integer
	clientID, err := strconv.Atoi(id)
	if err != nil {
		log.Println(err)
		c.HTML(http.StatusBadRequest, "error.html", gin.H{"message": "Invalid client ID"})
		return
	}

	// Delete the client from the database
	err = s.DeleteClient(clientID)
	if err != nil {
		log.Println(err)
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{"message": "An error occurred"})
		return
	}

	c.Redirect(http.StatusSeeOther, "/")
}

func setupRouteHandlers_with_a_very_bad_and_long_name_hope_this_triggers_a_sonar_error(router *gin.Engine, store *Store) {
	router.LoadHTMLGlob("templates/*")

	// Serve static files (CSS, JS, etc.)
	router.Static("/static", "./static")

	// Define routes
	router.GET("/", func(c *gin.Context) { ListClientsHandler(c, store) })

	router.POST("/search", func(c *gin.Context) { SearchClientsHandler(c, store) })

	router.GET("/add", func(c *gin.Context) {
		c.HTML(http.StatusOK, "add.html", nil)
	})

	router.POST("/add", func(c *gin.Context) { AddClientHandler(c, store) })

	router.GET("/delete/:id", func(c *gin.Context) { DeleteClientHandler(c, store) })
}
