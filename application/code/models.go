package main

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

// Client struct
type Client struct {
	ID            int
	Nom           string
	Prenom        string
	DateNaissance string
	Adresse       string
	CodePostal    string
	Ville         string
}

type Store struct {
	database *sql.DB
}
