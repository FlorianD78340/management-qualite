CREATE TABLE app.clients (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    date_naissance DATE,
    adresse VARCHAR(255),
    code_postal VARCHAR(10),
    ville VARCHAR(255)
);
